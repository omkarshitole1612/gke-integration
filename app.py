from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    print("In hello world function")
    return 'Hello, Docker!'
